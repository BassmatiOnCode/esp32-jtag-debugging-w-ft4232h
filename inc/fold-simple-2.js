//	Fold
// Dependencies
//		Compatibility

/* eslint-disable no-redeclare */

		// Root object
if ( typeof Synesis === "undefined" ) Synesis = { } ;
Synesis.isIndexPage =  document.location.pathname.endsWith( "index.htm"  );

		// Collapsible block functions
( function initCollapsibleBlock ( ) {
	
	if ( typeof Synesis.CollapsibleBlock === "undefined" ) Synesis.CollapsibleBlock = { } ;
	
	var errorInfo = "topic.js, initCollapsibleBlocks() : ";

		// Update the block style when the transition finished.
	Synesis.CollapsibleBlock.transitionEnd = function ( evt ) {
		evt = evt || window.event;
		/*
		if ( this.style.height !== "0px" ) this.style.height = "auto";
		*/
		evt.stopPropagation();
		return false;
	}

		// Expand a single block
	Synesis.CollapsibleBlock.expand = function ( controller ) {
		// Returns true if the controller state actually changed.
		// Expand the block to the required height
		if ( controller.getAttribute( "cbs" ) === "0" ) return false;
		/*
		// Controllers may not already be initialized.
		if ( controller.Synesis && controller.Synesis.block ) {
			// Set style height temporarily to "auto" to get the required numerical value for the transition.
			controller.Synesis.block.style.height = "auto";
			var height = window.getComputedStyle( controller.Synesis.block ).getPropertyValue( "height" );
			controller.Synesis.block.style.height = "0px";
			// Set the style height on a new script engine loop.
			window.setTimeout( function( ) { 
				controller.Synesis.block.style.height = height; 
			} , 100 );
		}
		*/
		// Update controller state.
		controller.setAttribute( "cbs", "0" );
		return true;
	};

		// Expand all collapsible blocks
	Synesis.CollapsibleBlock.expandAll = function ( ) {
		var controllers = document.querySelectorAll( "[cbs]" );
		for ( var i = 0 ; i < controllers.length ; i ++ ) Synesis.CollapsibleBlock.expand( controllers[ i ] );
	} ;

		// Collapse a block
	Synesis.CollapsibleBlock.collapse = function( controller ) {
		/*
		// Set style height to ensure a numerical transition start value (instead of "auto").
		controller.Synesis.block.style.height = window.getComputedStyle( controller.Synesis.block ).getPropertyValue( "height" );
		// Collapse the block on a new script engine loop.
		window.setTimeout( function ( ) { controller.Synesis.block.style.height = "0px"; }, 100 );		
		// Set the new controller state.
		*/
		controller.setAttribute( "cbs", "1" );
	};

		// Collapse all collapsible blocks
	Synesis.CollapsibleBlock.collapseAll = function ( ) {
		var controllers = document.querySelectorAll( "[cbs]" );
		for ( var i = 0 ; i < controllers.length ; i ++ ) Synesis.CollapsibleBlock.collapse( controllers[ i ] );
	} ;

		// Click event handler for the collapsible block controllers.
	Synesis.CollapsibleBlock.clickHandler = function ( evt ) { 
		evt = evt || window.event;
		// Bypass if the controller icon has not been hit.
		if ( evt.offsetX > 40 ) return;
		switch ( this.getAttribute( "cbs" ) ) {
		case "0" :  // block is expanded
				Synesis.CollapsibleBlock.collapse( this );
				break;
		case "1" :  // block is collapsed.
				Synesis.CollapsibleBlock.expand( this );
				break;
		default:
			console.error ( errorInfo + "Illegal controller state, reset to 0." );
			controller.setAttribute( "cbs", "0" );
			/*
			controller.Synesis.block.style.height = "auto";
			*/
		}
		// Declare the event handled.
		Synesis.Compatibility.stopEventPropagation( evt );
		evt.preventDefault();
		return false;
	} ;

		// Expand all blocks that contain the link target element
	Synesis.CollapsibleBlock.expandParentBlocks = function ( target ) {
		// Returns true if at least one block was expanded.
		var result = false;
		// Loop through the parent elements.
		while ( target && target.tagName !== "BODY" ) {
			// Look for a collapsible block controller.
			var controller = target;
			if ( ! controller.hasAttribute( "cbs" )) controller = target.previousElementSibling;
			// If there is one, expand its block.
			if ( controller && controller.hasAttribute( "cbs" )) result |= Synesis.CollapsibleBlock.expand( controller );
			// Ascend to the parent node.
			target = target.parentNode;
		}
		return result;
	} ;

		// Initialize collapsible blocks in the document.
	Synesis.CollapsibleBlock.initDocument = function ( target ) {
		// *	
		// *	Must be called when the document has been loaded competely.
		// *	Method deletes itself when done.
		
		// Collect the collabsible block controllers on the page
		// and exit if there is nothing to do.
		var controllers = document.querySelectorAll( "[cbs]" );
		if ( controllers.length === 0 ) return;

		// Expand all blocks that contain the navigation target element 
		// (id in the hash), and the link target itself.
		// Only the block controller status will be changed here.
		var hash = document.location.hash;
		if ( hash ) {
			var linkTarget = document.getElementById( hash.substr( 1 ));
			Synesis.CollapsibleBlock.expandParentBlocks( linkTarget );
			window.setTimeout ( function( ) { 
				linkTarget.scrollIntoView( { behavior:"smooth" } );
			} , 100 );
		}

		var forcedState; 
		if ( Synesis.Search && Synesis.Search.params.expand && Synesis.Search.params.expand === "all" ) forcedState = "0" ;
		if ( Synesis.Search && Synesis.Search.params.collapse && Synesis.Search.params.collapse === "all" ) forcedState = "1" ;

		// Initialize controllers and blocks.
		for ( var i = 0 ; i < controllers.length ; i ++ ) {
			var controller = controllers[ i ];
			// Create the root element for user-defined members.
			if ( typeof controller.Synesis === "undefined" ) controller.Synesis = { } ;
			// Find the associated collapsible block.
			// TODO: Skip comment elements.
			var block = controller.Synesis.block = controller.nextElementSibling;
			if ( typeof forcedState !== "undefined" ) controller.setAttribute( "cbs" , forcedState );
			// Collapse blocks if the controller status indicates so.
			/*
			if ( controller.getAttribute( "cbs" ) === "1" ) block.style.height = "0px" ;
			*/
			// Register event handlers.
			controller.addEventListener( "click", Synesis.CollapsibleBlock.clickHandler.bind( controller ));
			/*
			controller.style.transition = "margin-top linear 1s, margin-bottom linear 1s";
			block.addEventListener( "transitionend", Synesis.CollapsibleBlock.transitionEnd.bind( block ));
			block.style.transition = "height linear 1s";
			*/
		}

		controllers = null;	// Release memory.

		// Modify links that point to an element in the document.
		var links = document.getElementsByTagName( "A" ) ;
		for ( var i = 0 ; i < links.length ; i ++ ) {
			var link = links[ i ];
			// Check whether the anchor element qualifies for monitoring.
			if ( link.pathname !== document.location.pathname ) continue;
			if ( ! link.hash ) continue;
			var target = document.getElementById( link.hash.substr( 1 )); 
			if ( ! target ) continue;
			// The link is qualified.
			link.addEventListener( "click", function( target, evt ) { 
				// Function scrolls the target element into view.
				// Prevent the default link action.
				evt = evt || window.event;
				evt.preventDefault( );
				var delay = Synesis.CollapsibleBlock.expandParentBlocks( target ) ? 1100 : 10;
				// Make sure the link target element is visible.
				window.setTimeout ( function ( ) { 
					target.scrollIntoView( { behavior:"smooth", block: "center" } );
					target.scrollIntoView( );
					} , delay );
				return false;
			}.bind ( link, target  )) ;
			continue;
		}
		links = null;
		// Job done, release ressources.
		delete Synesis.CollapsibleBlock.initDocument;
	} ;

} ) ( ) ;
