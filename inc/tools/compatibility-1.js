// Compatibility helper functions

if ( typeof Synesis === "undefined" ) Synesis = { } ;

( function initCompatibility ( ) {
	
	if ( typeof Synesis.Compatibility === "undefined" ) Synesis.Compatibility = { } ;

	Synesis.Compatibility.stopEventPropagation = function ( evt ) {
		evt = evt || window.event;
		if ( evt.stopPropagation ) evt.stopPropagation( );
		else evt.cancelBubble = true;
	} ;
} ) ( ) ;
