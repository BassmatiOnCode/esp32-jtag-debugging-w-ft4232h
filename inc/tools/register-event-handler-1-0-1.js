/*
 *		.../tools/register-event-handler-1-0-1.js
 *			The event name should be given without "on" prefix, it is added in this code if requered.
 *		2018-05-29 usp - extracted from global-5.js
 *		2018-11-18 usp - Version 1-0-1 : Moved to the Tools namepace. Use strict added.
 */

"use strict" ;
if ( typeof Synesis === "undefined" ) var Synesis = { } ;
if ( typeof Synesis.Tools === "undefined" ) Synesis.Tools = { } ;

Synesis.Tools.registerEventHandler = function ( targetObject, eventName, eventHandler, capture ) 
{
	if ( ! targetObject ) return;
	if ( targetObject.addEventListener ) targetObject.addEventListener ( eventName, eventHandler, capture );
	else if ( targetObject.attachEvent ) targetObject.attachEvent( "on" + eventName, eventHandler );
	else targetObject[ "on" + eventName ] = eventHandler ;
} ;
