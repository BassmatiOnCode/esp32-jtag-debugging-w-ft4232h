/*
 *		.../tools/setup-event-1-0-1.js
 *			Compatibility function, standardizes event source (target) access. 
 *		Notes
 *			No strict mode because evt.target is read-only in strict mode.
 *		Dependencies
 *			None
 *		Dependents
 *			.../tree/collapsible-tree-2-0-0.js
 *		2016-06-14 usp - IE8 cannot handle evt.target.
 *		2018-06-01 usp - Streamlined, changed to evt.target.
 *		2018-11-15 usp - Moved to the Tools namespace.
 */

if ( typeof Synesis === "undefined" ) var Synesis = { };
if ( typeof Synesis.Tools === "undefined" ) Synesis.Tools = { };

Synesis.Tools.setupEvent = function ( evt ) 
{
	evt = evt || window.event;
	evt.target = evt.target || evt.srcElement;
	return evt;
} ;
