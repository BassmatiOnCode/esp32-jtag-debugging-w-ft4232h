# ESP32 JTAG Debugging w FT4232H

How to use an FT4232HL board as JTAG interface to ESP32 modules.

Recently I found a nice FT4232HL breakout and JTAG interface board. Unfortunately, the documentation download link didn't work for me, because my Chinese language is somewhat, well, underdeveloped. Fortunately I was able to find a Git repository with some resources (schematics, source code, tools, instructions). The manual was - of course - in Chinese. The images alone didn't make sense to me, so I decided to translate it and get the interface to work. Here I want to share my findings.



